<div style="background: #FbFbFb;">

<?php
echo '<hr><br>';
echo '<h4>1</h4><br>';

function red($x){

    ?>
    <button style="border: 2px solid #fff;  height: 50px; width: 50px; padding: 3px; background: #ff3236;"><?= $x;?></button>
    <?php
}
function biru($i,$y){
    ?>
    <button style="border: 2px solid #fff;  height: 50px; width: 50px; padding: 3px; background: #6131ff;"><?= $i.'-'.$y?></button>
    <?php
}

function nl($nomor){
    echo "<br>";
    echo '<hr><br>';
    echo '<h4>'.$nomor.'</h4><br>';
}

nl('Logic 1 : Soal 1');

$n = 9;
for ($i=0; $i < $n; $i++) { 
    for ($y=0; $y < $n; $y++) { 
       if ($i == $y) {
            
            red($i);
        }else{
            biru($i,$y);
        }

    }
    echo "<br>";
}


nl('Logic 1 : Soal 2');

$n = 9;
for ($i=0; $i < $n; $i++) { 
    for ($y=0; $y < $n; $y++) { 
       if ($i == ($n-$y-1) ) {
            
            red($i);
        }else{
            biru($i,$y);
        }

    }
    echo "<br>";
}

nl('Logic 1 : soal 3');
$n = 9;
for ($i=0; $i < $n; $i++) { 
    for ($y=0; $y < $n; $y++) { 
       if ($i == ($n-$y-1) || $i == $y ) {
            
            red($i);
        }else{
            biru($i,$y);
        }

    }
    echo "<br>";
}

nl('Logic 1 : soal 4');
$n = 9;
for ($i=0; $i < $n; $i++) { 
    for ($y=0; $y < $n; $y++) { 
       if ($i == ($n-$y-1) || $i == $y || $i == 4 ||$y == 4 ) {
            
            red($i);
        }else{
            biru($i,$y);
        }

    }
    echo "<br>";
}

nl('Logic 1 : Soal 5');
$n = 9;
for ($i=0; $i < $n; $i++) { 
    for ($y=0; $y < $n; $y++) { 
        if ($y <= $i) {
            red($i);
        }else{
            biru($i,$y);
        }
    }
    echo "<br>";
}

nl('Logic 1 : Soal 6');
$n = 9;
for ($i=0; $i < $n; $i++) { 
    for ($y=0; $y < $n; $y++) { 
        if ($y >= $n-$i-1) {
            red($i);
        }else{
            biru($i,$y);
        }
    }
    echo "<br>";
}

nl('Logic 1 : Soal 7');
$n = 9;
for ($i=0; $i < $n; $i++) { 
    for ($y=0; $y < $n; $y++) { 
        if (($i <= $y)&&($y <= ($n-$i-1))||($i >= $y)&&($i >= ($n-$y-1))) {
            red($i);
        }else{
            biru($i,$y);
        }
    }
    echo "<br>";
}

nl('Logic 1 : Soal 8');
$n = 9;
for ($i=0; $i < $n; $i++) { 
    for ($y=0; $y < $n; $y++) { 
        if (($y <= $i)&&($i <= ($n-$y-1))||($y >= $i)&&($y >= ($n-$i-1))) {
            red($i);
        }else{
            biru($i,$y);
        }
    }
    echo "<br>";
}
?>
</div>