<div style="background: #FbFbFb;">
	<?php
/**
* 
*/

/**
* 
*/
require_once('KotakLogic.php');

$kotak = new KotakLogic();

$n = $kotak->n;

$kotak->nl('Logic 2 : Soal 1');
for ($i=0; $i < $n; $i++) { 
	for ($y=0; $y < $n; $y++) { 
		if ($i == $y) {
			$kotak->red($i+1+$i);
		}else{
			$kotak->biru($i,$y);
		}
	}echo '<br>';
}

$kotak->nl('Logic 2 : Soal 2');
for ($i=0; $i < $n; $i++) { 
	for ($y=0; $y < $n; $y++) { 
		if ($i == $n-1-$y) {
			$kotak->red($y*2);
		}else{
			$kotak->biru($i,$y);
		}
	}echo '<br>';
}

$kotak->nl('Logic 2 : Soal 3');
for ($i=0; $i < $n; $i++) { 
	for ($y=0; $y < $n; $y++) { 
		if ($i == $n-1-$y) {
			$kotak->red($y*2);
		}elseif($i==$y){
			$kotak->red($i+1+$i);
		}else{
			$kotak->biru($i,$y);
		}
	}echo '<br>';
}


$kotak->nl('Logic 2 : Soal 4');
for ($i=0; $i < $n; $i++) { 
	for ($y=0; $y < $n; $y++) { 
		if ($i == $n-1-$y ) {
			$kotak->red($y*2);
		}elseif($i==$y){
			$kotak->red($i+1+$i);
		}elseif($y==4){
			$kotak->red($i+$i+1);
		}elseif($i==4){
			$kotak->red($y*2);
		}else{
			$kotak->biru($i,$y);
		}
	}echo '<br>';
}

$kotak->nl('Logic 2 : Soal 5');
for ($i=0; $i < $n; $i++) { 
	for ($y=0; $y < $n; $y++) { 
		if ($i>=$y) {
			$kotak->red($y+1+$y);
		}else{
			$kotak->biru($i,$y);
		}
	}echo '<br>';
}

$kotak->nl('Logic 2 : Soal 6');
for ($i=0; $i < $n; $i++) { 
	for ($y=0; $y < $n; $y++) { 
		if ($i>=$n-$y-1) {
			$kotak->red(16-($i*2));
		}else{
			$kotak->biru($i,$y);
		}
	}echo '<br>';
}

$kotak->nl('Logic 2 : Soal 7');
for ($i=0; $i < $n; $i++) { 
	for ($y=0; $y < $n; $y++) { 
		if ($i == $y) {
			$kotak->red($i+1+$i);
		}elseif ($i==$n-$y-1) {
			$kotak->red($y*2);
		}elseif($i <= ($n-$y-1) && $y >=$i){
			$kotak->red('A');
		}elseif($y >= ($n-$i-1) && $y <= $i){
			$kotak->red('B');
		}else {
			$kotak->biru($i,$y);
		}
	}echo '<br>';
}

$kotak->nl('Logic 2 : Soal 8');
for ($i=0; $i < $n; $i++) { 
	for ($y=0; $y < $n; $y++) { 
		if ($i == $y) {
			$kotak->red($i+1+$i);
		}elseif ($i==$n-$y-1) {
			$kotak->red($y*2);
		}elseif($i >= $y && $i <=$n-$y-1){
			$kotak->red('A');
		}elseif($i >= ($n-$y-1) && $i <=$y){
			$kotak->red('B');
		}else {
			$kotak->biru($i,$y);
		}
	}echo '<br>';
}


/*============== Logic 2 : Soal 9 ===============*/

$kotak->nl('Logic 2 : Soal 9');
$nx = 9;
$akar5 = sqrt(5);


for ($i=1; $i <= $nx; $i++) { 
	for ($y=1; $y <= $nx; $y++) {

		$Phi = pow((1+$akar5)/2,$y);
		$vhi = pow((1-$akar5)/2,$y);
		$formula = ($Phi-$vhi)/$akar5; 
		$kotak->red($formula);	

	}echo "<br>";
}



/*============== Logic 2 : Soal 10 ===============*/
$kotak->nl('Logic 2 : Soal 10');
$nx = 9;
$akar5 = sqrt(5);


for ($i=1; $i <= $nx; $i++) { 
	for ($y=1; $y <= $nx; $y++) {

		$Phi = pow((1+$akar5)/2,$i);
		$vhi = pow((1-$akar5)/2,$i);
		$formula = ($Phi-$vhi)/$akar5; 
		$kotak->red($formula);	

	}echo "<br>";
}


$kotak->nl('Logic 2 : Soal PRetest');
$nx = 9;
$akar5 = sqrt(5);
/*============== Logic 2 : Pre test ===============*/


for ($i=1; $i <= $nx; $i++) { 
	for ($y=1; $y <= $nx; $y++) {
		if ($y==5) {
			$Phi = pow((1+$akar5)/2,$i);
			$vhi = pow((1-$akar5)/2,$i);
			$formula = ($Phi-$vhi)/$akar5; 
			$kotak->red($formula);
		}elseif ($i==$nx-$y+1 && $i >5) {
			$Phi = pow((1+$akar5)/2,$i);
			$vhi = pow((1-$akar5)/2,$i);
			$formula = ($Phi-$vhi)/$akar5; 
			$kotak->red($formula);
		}elseif ($i==$y&&$y>5) { 
			$Phi = pow((1+$akar5)/2,$y);
			$vhi = pow((1-$akar5)/2,$y);
			$formula = ($Phi-$vhi)/$akar5; 
			$kotak->red($formula);
		} else{
			$kotak->biru($i,$y);
		}
	}echo "<br>";
}

$kotak->nl('Logic 2 : Soal PR');
$nx = 9;
$akar5 = sqrt(5);


for ($i=1; $i <= $nx; $i++) { 
	for ($y=1; $y <= $nx; $y++) {
		if ($y==5) {
			$Phi = pow((1+$akar5)/2,$i);
			$vhi = pow((1-$akar5)/2,$i);
			$formula = ($Phi-$vhi)/$akar5; 
			$kotak->red($formula);
		}elseif ($i==$nx-$y+1 && $i >5) {
			$Phi = pow((1+$akar5)/2,$y);
			$vhi = pow((1-$akar5)/2,$y);
			$formula = ($Phi-$vhi)/$akar5; 
			$kotak->red($formula);
		}elseif ($i==$y&&$y>5) { 
			$Phi = pow((1+$akar5)/2,$n-$y+1);
			$vhi = pow((1-$akar5)/2,$n-$y+1);
			$formula = ($Phi-$vhi)/$akar5; 
			$kotak->red($formula);
		} else{
			$kotak->biru($i,$y);
		}
	}echo "<br>";
}
?>
</div>